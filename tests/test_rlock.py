import asyncio
import pytest

from asyncio_rlock import RLock


@pytest.mark.asyncio
async def test_rlock():
    lock = RLock()

    step_into_first = False
    step_into_second = False
    step_into_third = False

    async with lock:
        step_into_first = True
        async with lock:
            step_into_second = True
            async with lock:
                step_into_third = True

    assert all([
        step_into_first,
        step_into_second,
        step_into_third,
    ])

    assert lock._depth == 0


@pytest.mark.asyncio
async def test_rlock_with_concurrency():
    lock = RLock()

    step_into_first = False
    step_into_second = False
    step_into_third = False
    container = []

    task = asyncio.create_task(concurrent_task(lock, container))

    async with lock:
        assert not container
        step_into_first = True
        async with lock:
            assert not container
            step_into_second = True
            async with lock:
                assert not container
                step_into_third = True

    assert all([
        step_into_first,
        step_into_second,
        step_into_third,
    ])
    assert not container

    await asyncio.sleep(1)
    assert container

    assert lock._depth == 0


async def concurrent_task(lock, container):
    async with lock:
        container.append(True)
